import React from 'react'
import { BrowserRouter as Router, Redirect, Route,Switch } from "react-router-dom";
import {Header} from './components/header'
import {Home} from './pages/home'
import { Register } from './pages/register'
import { Login } from './pages/login'
import { Posts } from './pages/posts'
import {getLocalStorageItem} from '../js/utils/localStorage'
// import { Layout } from "./components/layout";
// import { AuthRoute } from './components/authRoute'

export const App = () => {

    return (
        <>
          
         <Router>
         <Header/> 
             <Switch>
                 <Route exact path='/' >{getLocalStorageItem('token') ? <Posts/> : <Home/>}</Route>
                 <Route exact path='/register' component={Register}/>
                 <Route exact path='/login'>{getLocalStorageItem('token') ? <Posts/> : <Login/>}</Route>
                 <Route exact  path='/posts'>{getLocalStorageItem('token') ? <Posts/> : <Redirect to='/'/>} </Route>                
                 {/* <Layout>
                    <AuthRoute path="/posts" component={Posts} />
                </Layout> */}
             </Switch>
         </Router>

        </>
    )
}