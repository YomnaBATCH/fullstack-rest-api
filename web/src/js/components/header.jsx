import React, { useEffect, useState } from 'react';
import { Link} from 'react-router-dom';

import {getLocalStorageItem, removeLocalStorageItem} from '../utils/localStorage'

export const Header = () => {
    const logOutHolder = () =>{
        removeLocalStorageItem('token')
        window.location.reload()
    }

        return (
            <div className="header-class">
              <h1>My Appliction</h1>
                  
                   <Link to='/register' className="link-header">Register</Link>
                   {
                    getLocalStorageItem('token') 
                      ?  
                      <Link to='/' className="link-header" onClick={logOutHolder}>Logout</Link>
                      :
                      <Link to='/login' className="link-header">login</Link>

                  }
              
            </div>
        )
    
}

