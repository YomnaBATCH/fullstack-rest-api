import React from 'react'

export const InputGroup = (props) => {
    const { handleChange,  label, type, required, minLength, maxLength } = props;

    return (
        <div  className="form-class" >
            <label>{label}</label>
            <input
                onChange={(e) => handleChange(e.target.value)}
                type={type}
                required={required}
                minLength={minLength}
                maxLength={maxLength}
            />
        </div>
    )
}