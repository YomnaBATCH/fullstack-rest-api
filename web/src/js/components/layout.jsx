  
import React from 'react'

const Layout = ({ children }) => {
    return (
        <div>
           
            <div style={{ display: 'flex', flexGrow: '1' }}>
                    {children}
            </div>
        </div>
    )
}

export { Layout }