
import React from 'react'
import ReactDOM from 'react-dom'

import {App} from './app'

const MOUNT_DOM = document.getElementById('app')

ReactDOM.render(
    <App/>,
    MOUNT_DOM,
    ()=>{
        console.log('in render method')
    }
)