import path from 'path'; // native avec js (@type/node), SO pas besoin l'insatller
import dotenv from 'dotenv'


// le fichier .env que on utilise il se retrouve à la racine de notre projet
const envPath = path.join(__dirname, '../../../../')
 dotenv.config({path: envPath + "./.env"})

 export const NODE_ENV = process.env.NODE_ENV

 export const API_BASE_URL = process.env.API_BASE_URL || 'http://localhost:3000/api/v1/' // pour regler undefined


