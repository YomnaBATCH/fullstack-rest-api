import React, { useState, useEffect} from 'react'
import {api} from '../utils/api'

export const Posts = (props)=>{

    const {title} = props;

    const [posts, setPosts]= useState([])
useEffect(async ()=>{
    const result = await api.get('posts')
   setPosts(result.data);
})

    return (
        <div >
            <h1>
                {title}
            </h1>
            <ul>
                {
                    posts.map( (post) => {
                        return (
                            <li key={post.id}>{post.title.toUpperCase()}</li>
                            
                        )
                    })
                }
            </ul>
        </div>
    )
}