import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'
import { InputGroup } from '../components/inputGroup'
import { SubmitButton } from '../components/submitButton'
import {api} from '../utils/api'
import {setLocalStorageItem} from '../utils/localStorage'

export const Login = () => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [message, setMessage] = useState("")
    const [error, setError] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [redirect, setRedirect] = useState(false)
    if (redirect) {
        return <Redirect to="/posts" />;
    }
    const handleSubmit = async (e) => {
        e.preventDefault();

        setError(null)
        setIsLoading(false)
        setMessage('')
        try {
            setIsLoading(true)
            const axiosResult = await api.post('users/authenticate', { email, password });
            axiosResult.status === 200 && setLocalStorageItem('token', axiosResult.data)
            setIsLoading(false)
            setRedirect(true)
            window.location.reload()
        }
        catch (err) {
            console.log('axios err :', err.response);
            setError(err.response?.data?.message);
            setIsLoading(false)
        }
    }
    return (
        <div className="container">
            <h1>Login page</h1>
            <p style={{ color: "green", fontStyle: "italic", fontWeight: "bold" }}>{message && message}</p>

            <p style={{ color: "red",fontStyle: "italic", fontWeight: "bold" }}>{error && error}</p>
            {isLoading && <p>Loading...</p>}
            <form onSubmit={handleSubmit} >
                <InputGroup handleChange={setEmail} label="Email" type="email" required />
                <InputGroup handleChange={setPassword} label="Password" type="password" required={true} minLength="1" maxLength="15" />

                <SubmitButton name="login"  className="button-class"/>
            </form>
        </div>
    )
}