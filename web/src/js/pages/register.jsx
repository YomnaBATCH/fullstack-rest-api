import React, { useState } from 'react'
import { InputGroup } from '../components/inputGroup'
import { SubmitButton } from '../components/submitButton'
import {api} from '../utils/api'

export const Register = () => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [name, setName]= useState("")
    const [error, setError] = useState(null)
    const [message, setMessage] = useState("")
   const [isLoading, setIsLoading] = useState(false)

    const handleSubmit = async (e) => {
        e.preventDefault();

        setError(null)
       setMessage('')
        setIsLoading(false)

        try {
            setIsLoading(true)
            const axiosResult = await api.post('users/new-user', { email, password, name });
           axiosResult.status === 200 && setMessage(axiosResult.data.message)
            setIsLoading(false)
        }
        catch (err) {
            console.log('axios err :', err.response);
            setError(err.response?.data?.message);
            setIsLoading(false)
        }
    }

    return (
        <div className="container">
            <h1>Register</h1>
            <p style={{ color: "green", fontStyle: "italic", fontWeight: "bold" }}>{message && message}</p>
            <p style={{ color: "red" }}>{error && error}</p>
            {isLoading && <p>Loading...</p>}
            <form onSubmit={handleSubmit}>
                <InputGroup handleChange={setEmail}  label="Email" type="email" required />
                <InputGroup handleChange={setPassword}  label="Password" type="password" required={true} minLength="1" maxLength="15" />
                <InputGroup handleChange={setName}  label="Name" type="text" />

                <SubmitButton name="Register" />
            </form>
        </div>
    )
}