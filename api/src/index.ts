import {createServer} from './server'
import { PORT, NODE_ENV } from './config';

const main = async () =>{

    const server = createServer();
    server.listen(PORT, ()=> {
        console.log(`Your server now listen the port ${PORT} in ${NODE_ENV}`);
    })

}
main();