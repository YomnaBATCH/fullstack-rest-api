import { prisma } from './database/index';
// import bcrypt from 'bcrypt'
import {Router, Request, Response} from 'express'

import {userRouter} from './module/user/userRouter'

const mainRouter: Router = Router();

// const saltRounds = 10;


 
mainRouter.get('/', (_: Request, res:Response)=>{
res.send('RRRRROUUUT');

})
mainRouter.use('/users', userRouter);

// ---------------------------------

mainRouter.get('/posts', async (_:Request, res:Response)=>{
    const allPosts = await prisma.post.findMany();
    res.json(allPosts)
})    
mainRouter.get('/posts/one-post/:id', async (req:Request, res:Response)=>{ 
    const id = parseInt(req.params.id) 

    const onePost = await prisma.post.findFirst({
        where:{
            id:id
        }
    });
    res.json(onePost)
}) 
mainRouter.post('/posts/new-post', async (req:Request, res:Response)=>{
    const {title, content, published, authorId }= req.body
  const newPost = await prisma.post.create({
    data: {
      title: title,
      content: content,
      published: published,
      authorId: authorId,
    }
  })
    res.json(newPost)
})  

//--------------users--------------------
mainRouter.get('/users', async (_:Request, res:Response)=>{
    const allUers = await prisma.user.findMany();
    res.json(allUers)
})
mainRouter.get('/users/one-user/:id', async (req:Request, res:Response)=>{ 
    const id = parseInt(req.params.id) 

    const oneUser = await prisma.user.findFirst({
        where:{
            id:id
        }
    });
    res.json(oneUser)
}) 
// mainRouter.post('/users/login', async (req:Request, res:Response)=>{ 
//     const {email, password}= req.body 

//     const oneUser = await prisma.user.findFirst({
//         where:{
//             email: email
//         }
//     })
//     if(oneUser){
//         bcrypt.compare(password, oneUser.password).then(result =>{
            
//                 if(result){
//                     res.status(200).json({message: 'Your are authorised'})
//                 }else{
//                     res.status(400).json({message: 'Please try again'})

//                 }
//         })
//     }else{
//         res.status(401).json({message: 'This account not exsit'})

//     }
    
// })
// mainRouter.post('/users/new-user', async (req:Request, res:Response)=>{
//     const {email, password, name }= req.body
//     const userExist = await prisma.user.findFirst({
//         where:{
//             email: email
//         }
//     })
//     if(userExist ){
//         res.status(401).json({message: "You should use another email"})
//     }
//     bcrypt.hash(password, saltRounds, async(err, hash)=>{
//         if(err) throw err;
//         const newUser = await prisma.user.create({
//             data: {
//                 email: email,
//                 password : hash,
//                 name: name,
//                 posts : undefined,
//             }
//         })
//     newUser ? res.status(200).json({newUser: newUser, message: "Your data are registered"}) : res.status(400).json({message: "You have to try again"})
    
//     })
    
    
// }) 
export {mainRouter}



