import path from 'path'; // native avec js pas besoin l'insatller
import dotenv from 'dotenv'
// le fichier .env que on utilise il se retrouve à la racine de notre projet
const envPath = path.join(__dirname, '../../')
 dotenv.config({path: envPath + "./.env"})


 export const PORT = process.env.PORT
 export const NODE_ENV = process.env.NODE_ENV
 export const APP_BASE_URL = process.env.APP_BASE_URL || '/api/v1/' // on est obligier de mettre autre chois car le premier peut etre undefined
 export const JWT_PASSPHRASE = process.env.JWT_PASSPHRASE || 'default passphrase'

