import express from 'express'
import cors from 'cors'
import {mainRouter} from './router'
import { APP_BASE_URL } from './config';

export const createServer =  () =>{
//initialiser le server
 const server = express();
 server.use(express.json()) // pour pouvoir lire body de type json
 //On indique les cors (qui peut emettre des call depuis notre API)
 server.use(cors({
    origin:"http://localhost:1234"
}))
 server.use(APP_BASE_URL,mainRouter);
 return server
}
