import { userProps } from "./userTypes";

export class UserRepo {
    private entities: any

    constructor(prismaEntitie:any){
        this.entities = prismaEntitie
    }
    public async createInRepo(userProps: userProps){
        console.log('in repoooooo',userProps )
     await this.entities.user.create({
         data:{
             email: userProps.email,
             password: userProps.password,
             name: userProps.name
         }
     })
     return 

    }
    public async userExist(email: string){
        const user = await this.entities.user.findUnique({
            where:{email:email}
        })
        if(user){
            return true
        }
        return false
    }
    public async getUserByEmail(email: string) {

        const result = await this.entities.user.findUnique({ where: { email: email } })

        return result
    }
}
