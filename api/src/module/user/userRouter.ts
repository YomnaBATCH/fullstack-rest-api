import { Router} from 'express'
import {createUserController} from './useCases/createUser' // importer l'instance from the index of this useCase
 import { loginController } from './useCases/login'

const userRouter:Router = Router()

 userRouter.get('/', ()=>{
     console.log('in user')
 })
 userRouter.post('/new-user', (req, res)=> createUserController.callCreateService(req, res))

//  //Authenticate
 userRouter.post('/authenticate', (req, res) => loginController.callLoginService(req, res))

 export { userRouter}