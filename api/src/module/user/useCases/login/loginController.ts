import { Login } from "./login";
import { Request, Response } from "express";

export class LoginController {
  private useCase: Login;

  constructor(loginCase: Login) {
    this.useCase = loginCase;
  }
  public async callLoginService(req: Request, res: Response) {
    try {
      const result = await this.useCase.tryToLogin(req.body);
      if (!result.success) {
        return res.status(400).json({ message: result.message });
      }

        let data = {
          name: result.payload?.name,
          email: result.payload?.email
        };
        return res
          .cookie("token", result.payload?.token, {
            maxAge: 900000,
            httpOnly: true,
          })
          .status(200)
          .json(data);
      
    } catch (err) {
      console.log(err);
      return res.status(400).json({ message: "there is an error" });
    }
  }
}
