  
import { UserRepo } from '../../userRepo'
import bcrypt from 'bcrypt'
import { sign } from 'jsonwebtoken'
import { JWT_PASSPHRASE } from '../../../../config'

import {userProps} from '../../userTypes'

//Equivalent to a specific service in a CRUD API
export class Login {
    private userRepo: UserRepo

    constructor(userRepo: UserRepo) {
        this.userRepo = userRepo
    }

    //This is what our use case will do
    public async tryToLogin(props: userProps) {
        try {
            const { email, password } = props;

            const user = await this.userRepo.getUserByEmail(email);

            if (!user) {
                return {
                    success: false,
                    message: "Email does not exist"
                }
            }
            const passwordMatches = await bcrypt.compare(password, user.password)

            if (!passwordMatches) {
                return {
                    success: false,
                    message: "Email or password missmatch"
                }
            }

            //Création de notre JWT token
            const jwtToken = sign({ id: user.id }, JWT_PASSPHRASE)

            return {
                success: true,
                payload: {
                    name: user.name,
                    email: user.email,
                    token: jwtToken
                }
            }

        } catch (err) {
            console.log('error :', err)
            return {
                success: false,
                message: err
            }
        }
    }
}