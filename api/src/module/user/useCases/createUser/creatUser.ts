// service...faire la logique de creation 
import {UserRepo} from '../../userRepo';

import bcrypt from 'bcrypt'
 const saltRound = 10;



export class CreateUser{
    private userRepo: UserRepo

    constructor(userRepo: UserRepo){
        this.userRepo= userRepo // on passe un parameter Repo de cette service
    }
    public async createService(props: any){
        try{
           const userAlreadyExist = await this.userRepo.userExist(props.email)

           if(userAlreadyExist){
               return {
                success : false,
                   message: 'User already exsit'
               }

           }
           const hashPassword = await bcrypt.hash(props.password, saltRound)
           props.password = hashPassword; // pour envoyer tout les bonnes data dans le props
           await this.userRepo.createInRepo(props)

           return{
               success : true,
               message: 'User is created correctly'
           }

        }catch(err){
            console.log(err);
            return{
                success: false,
                message: err
            }
            
        }
    }
}