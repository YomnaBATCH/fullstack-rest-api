// instancer tout
import { prisma } from '../../../../database/index'
import {UserRepo} from '../../userRepo'
import {CreateUser} from './creatUser' // service
import { CreateUserController } from './createUserController' // cotroller of this useCase
const userRepo = new UserRepo(prisma) // we pass instance of prisma which cis created in the folder database
const createUser = new CreateUser(userRepo) // we pass instance of Repo in the argument to have instance of service
const createUserController = new CreateUserController(createUser) // we pass insatance of service like argument
export {createUser, createUserController}