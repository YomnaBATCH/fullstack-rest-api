// il reeçvoit une reqete et il envoie une reponse( gere la logique HTTP)


import {CreateUser} from './creatUser' // service
import {Request, Response} from 'express'

export class CreateUserController {
    private useCase : CreateUser;

    constructor (createUser : CreateUser ){
        this.useCase = createUser;

    }
    public async callCreateService(req:Request, res: Response){
        try{
            // appeler le service

            const result = await this.useCase.createService(req.body)

            if(!result?.success ){
                return res.status(400).json({message: result?.message})
            }

            return res.status(201).json({message: result?.message})
        }catch(err){
            console.log(err);
            return res.status(400).json({message: err})
            
        }
    }
}